# service
Чтобы скопировать репозиторий к себе для работы, вам нужно следовать [этим инструкциям](https://docs.github.com/en/github/creating-cloning-and-archiving-repositories/creating-a-repository-on-github/duplicating-a-repository#mirroring-a-repository-in-another-location).

## Описание
Репозиторий настроен на автоматический запуск `Pipeline` при внесении изменений в папку `App`, ветки `master`.
1. Запуск теста 
2. Сборка контейнера и сохранение на Docker Hub
3. Деплой последней сборки на сервер 

## Как указать версию сборки для деплоя:
1. Перейти в паку `service`
2. Открыть файл `docker-compose.yaml`
3. В разделе `app` указать первые 8 символов коммита с необходимой сборкой.   
   Пример:   
   ```yaml
   app:
    image: ulitka/skillbox_app:b7bc1664
   ```
4. Сохранить внесённые изменения.

## Как выполнить деплой сборки на сервер в ручную:  
1. Открыть терминал "shell"
2. Перейти в папку Ansible:
   ```shell
   cd service/Ansible
   ```
3. Выполнить команду:
   ```shell
   ansible-playbook -b play.yml -v
   ```
5. По окончанию выполнения команды, проверить доступность приложения по ссылке: http://www.my-site-top.ru/

Информация об авторе
------------------

- Электронная почта: <petrukhin.ru@yandex.ru>
